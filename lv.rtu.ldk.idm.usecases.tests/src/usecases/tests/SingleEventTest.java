/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package usecases.tests;

import usecases.SingleEvent;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Single Event</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class SingleEventTest extends EventTest {

	/**
	 * Constructs a new Single Event test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SingleEventTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Single Event test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected SingleEvent getFixture() {
		return (SingleEvent)fixture;
	}

} //SingleEventTest
