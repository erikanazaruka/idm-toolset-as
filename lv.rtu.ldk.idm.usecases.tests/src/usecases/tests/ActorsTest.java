/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package usecases.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import usecases.Actors;
import usecases.UsecasesFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Actors</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class ActorsTest extends TestCase {

	/**
	 * The fixture for this Actors test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Actors fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ActorsTest.class);
	}

	/**
	 * Constructs a new Actors test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActorsTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Actors test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(Actors fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Actors test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Actors getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(UsecasesFactory.eINSTANCE.createActors());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //ActorsTest
