/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package usecases.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import usecases.UseCases;
import usecases.UsecasesFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Use Cases</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class UseCasesTest extends TestCase {

	/**
	 * The fixture for this Use Cases test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UseCases fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(UseCasesTest.class);
	}

	/**
	 * Constructs a new Use Cases test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UseCasesTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Use Cases test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(UseCases fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Use Cases test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UseCases getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(UsecasesFactory.eINSTANCE.createUseCases());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //UseCasesTest
