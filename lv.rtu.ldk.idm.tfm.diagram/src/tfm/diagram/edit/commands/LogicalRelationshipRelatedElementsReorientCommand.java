package tfm.diagram.edit.commands;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.emf.type.core.commands.EditElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.ReorientReferenceRelationshipRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.ReorientRelationshipRequest;

import tfm.LogicalRelationship;
import tfm.TopologicalRelationship;
import tfm.diagram.edit.policies.TFMBaseItemSemanticEditPolicy;

/**
 * @generated
 */
public class LogicalRelationshipRelatedElementsReorientCommand extends
		EditElementCommand {

	/**
	 * @generated
	 */
	private final int reorientDirection;

	/**
	 * @generated
	 */
	private final EObject referenceOwner;

	/**
	 * @generated
	 */
	private final EObject oldEnd;

	/**
	 * @generated
	 */
	private final EObject newEnd;

	/**
	 * @generated
	 */
	public LogicalRelationshipRelatedElementsReorientCommand(
			ReorientReferenceRelationshipRequest request) {
		super(request.getLabel(), null, request);
		reorientDirection = request.getDirection();
		referenceOwner = request.getReferenceOwner();
		oldEnd = request.getOldRelationshipEnd();
		newEnd = request.getNewRelationshipEnd();
	}

	/**
	 * @generated
	 */
	public boolean canExecute() {
		if (false == referenceOwner instanceof LogicalRelationship) {
			return false;
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_SOURCE) {
			return canReorientSource();
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_TARGET) {
			return canReorientTarget();
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected boolean canReorientSource() {
		if (!(oldEnd instanceof TopologicalRelationship && newEnd instanceof LogicalRelationship)) {
			return false;
		}
		return TFMBaseItemSemanticEditPolicy.getLinkConstraints()
				.canExistLogicalRelationshipRelatedElements_4009(
						getNewSource(), getOldTarget());
	}

	/**
	 * @generated
	 */
	protected boolean canReorientTarget() {
		if (!(oldEnd instanceof TopologicalRelationship && newEnd instanceof TopologicalRelationship)) {
			return false;
		}
		return TFMBaseItemSemanticEditPolicy.getLinkConstraints()
				.canExistLogicalRelationshipRelatedElements_4009(
						getOldSource(), getNewTarget());
	}

	/**
	 * @generated
	 */
	protected CommandResult doExecuteWithResult(IProgressMonitor monitor,
			IAdaptable info) throws ExecutionException {
		if (!canExecute()) {
			throw new ExecutionException(
					"Invalid arguments in reorient link command"); //$NON-NLS-1$
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_SOURCE) {
			return reorientSource();
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_TARGET) {
			return reorientTarget();
		}
		throw new IllegalStateException();
	}

	/**
	 * @generated
	 */
	protected CommandResult reorientSource() throws ExecutionException {
		getOldSource().getRelatedElements().remove(getOldTarget());
		getNewSource().getRelatedElements().add(getOldTarget());
		return CommandResult.newOKCommandResult(referenceOwner);
	}

	/**
	 * @generated
	 */
	protected CommandResult reorientTarget() throws ExecutionException {
		getOldSource().getRelatedElements().remove(getOldTarget());
		getOldSource().getRelatedElements().add(getNewTarget());
		return CommandResult.newOKCommandResult(referenceOwner);
	}

	/**
	 * @generated
	 */
	protected LogicalRelationship getOldSource() {
		return (LogicalRelationship) referenceOwner;
	}

	/**
	 * @generated
	 */
	protected LogicalRelationship getNewSource() {
		return (LogicalRelationship) newEnd;
	}

	/**
	 * @generated
	 */
	protected TopologicalRelationship getOldTarget() {
		return (TopologicalRelationship) oldEnd;
	}

	/**
	 * @generated
	 */
	protected TopologicalRelationship getNewTarget() {
		return (TopologicalRelationship) newEnd;
	}
}
