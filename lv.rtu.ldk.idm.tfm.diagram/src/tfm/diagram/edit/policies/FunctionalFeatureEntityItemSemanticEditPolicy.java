package tfm.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.commands.DestroyReferenceCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyReferenceRequest;

import tfm.diagram.providers.TFMElementTypes;

/**
 * @generated
 */
public class FunctionalFeatureEntityItemSemanticEditPolicy extends
		TFMBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public FunctionalFeatureEntityItemSemanticEditPolicy() {
		super(TFMElementTypes.FunctionalFeatureEntity_4004);
	}

	/**
	 * @generated
	 */
	protected Command getDestroyReferenceCommand(DestroyReferenceRequest req) {
		return getGEFWrapper(new DestroyReferenceCommand(req));
	}

}
