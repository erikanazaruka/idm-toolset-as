package tfm.diagram.providers;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.service.AbstractProvider;
import org.eclipse.gmf.runtime.common.core.service.IOperation;
import org.eclipse.gmf.runtime.common.ui.services.parser.GetParserOperation;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParser;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParserProvider;
import org.eclipse.gmf.runtime.common.ui.services.parser.ParserService;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.ui.services.parser.ParserHintAdapter;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.parsers.EnumParser;

import tfm.TfmPackage;
import tfm.diagram.edit.parts.ActorDescriptionEditPart;
import tfm.diagram.edit.parts.CycleOrderEditPart;
import tfm.diagram.edit.parts.FunctionalFeatureDescriptionEditPart;
import tfm.diagram.edit.parts.FunctionalFeatureIdEditPart;
import tfm.diagram.edit.parts.LogicalRelationshipIdEditPart;
import tfm.diagram.parsers.MessageFormatParser;
import tfm.diagram.part.TFMVisualIDRegistry;

/**
 * @generated
 */
public class TFMParserProvider extends AbstractProvider implements
		IParserProvider {

	/**
	 * @generated
	 */
	private IParser logicalRelationshipOperation_5001Parser;

	/**
	 * @generated
	 */
	private IParser getLogicalRelationshipOperation_5001Parser() {
		if (logicalRelationshipOperation_5001Parser == null) {
			EAttribute editableFeature = TfmPackage.eINSTANCE
					.getLogicalRelationship_Operation();
			EnumParser parser = new EnumParser(editableFeature);
			logicalRelationshipOperation_5001Parser = parser;
		}
		return logicalRelationshipOperation_5001Parser;
	}

	/**
	 * @generated
	 */
	private IParser cycleOrder_5006Parser;

	/**
	 * @generated
	 */
	private IParser getCycleOrder_5006Parser() {
		if (cycleOrder_5006Parser == null) {
			EAttribute[] features = new EAttribute[] { TfmPackage.eINSTANCE
					.getCycle_Order() };
			MessageFormatParser parser = new MessageFormatParser(features);
			cycleOrder_5006Parser = parser;
		}
		return cycleOrder_5006Parser;
	}

	/**
	 * @generated
	 */
	private IParser actorDescription_5002Parser;

	/**
	 * @generated
	 */
	private IParser getActorDescription_5002Parser() {
		if (actorDescription_5002Parser == null) {
			EAttribute[] features = new EAttribute[] { TfmPackage.eINSTANCE
					.getActor_Description() };
			MessageFormatParser parser = new MessageFormatParser(features);
			actorDescription_5002Parser = parser;
		}
		return actorDescription_5002Parser;
	}

	/**
	 * @generated
	 */
	private IParser functionalFeatureId_5004Parser;

	/**
	 * @generated
	 */
	private IParser getFunctionalFeatureId_5004Parser() {
		if (functionalFeatureId_5004Parser == null) {
			EAttribute[] features = new EAttribute[] { TfmPackage.eINSTANCE
					.getFunctionalFeature_Id() };
			MessageFormatParser parser = new MessageFormatParser(features);
			functionalFeatureId_5004Parser = parser;
		}
		return functionalFeatureId_5004Parser;
	}

	/**
	 * @generated
	 */
	private IParser functionalFeatureDescription_5005Parser;

	/**
	 * @generated
	 */
	private IParser getFunctionalFeatureDescription_5005Parser() {
		if (functionalFeatureDescription_5005Parser == null) {
			EAttribute[] features = new EAttribute[] { TfmPackage.eINSTANCE
					.getFunctionalFeature_Description() };
			MessageFormatParser parser = new MessageFormatParser(features);
			functionalFeatureDescription_5005Parser = parser;
		}
		return functionalFeatureDescription_5005Parser;
	}

	/**
	 * @generated
	 */
	protected IParser getParser(int visualID) {
		switch (visualID) {

		case LogicalRelationshipIdEditPart.VISUAL_ID:
			return getLogicalRelationshipOperation_5001Parser();
		case CycleOrderEditPart.VISUAL_ID:
			return getCycleOrder_5006Parser();
		case ActorDescriptionEditPart.VISUAL_ID:
			return getActorDescription_5002Parser();
		case FunctionalFeatureIdEditPart.VISUAL_ID:
			return getFunctionalFeatureId_5004Parser();
		case FunctionalFeatureDescriptionEditPart.VISUAL_ID:
			return getFunctionalFeatureDescription_5005Parser();
		}
		return null;
	}

	/**
	 * Utility method that consults ParserService
	 * @generated
	 */
	public static IParser getParser(IElementType type, EObject object,
			String parserHint) {
		return ParserService.getInstance().getParser(
				new HintAdapter(type, object, parserHint));
	}

	/**
	 * @generated
	 */
	public IParser getParser(IAdaptable hint) {
		String vid = (String) hint.getAdapter(String.class);
		if (vid != null) {
			return getParser(TFMVisualIDRegistry.getVisualID(vid));
		}
		View view = (View) hint.getAdapter(View.class);
		if (view != null) {
			return getParser(TFMVisualIDRegistry.getVisualID(view));
		}
		return null;
	}

	/**
	 * @generated
	 */
	public boolean provides(IOperation operation) {
		if (operation instanceof GetParserOperation) {
			IAdaptable hint = ((GetParserOperation) operation).getHint();
			if (TFMElementTypes.getElement(hint) == null) {
				return false;
			}
			return getParser(hint) != null;
		}
		return false;
	}

	/**
	 * @generated
	 */
	private static class HintAdapter extends ParserHintAdapter {

		/**
		 * @generated
		 */
		private final IElementType elementType;

		/**
		 * @generated
		 */
		public HintAdapter(IElementType type, EObject object, String parserHint) {
			super(object, parserHint);
			assert type != null;
			elementType = type;
		}

		/**
		 * @generated
		 */
		public Object getAdapter(Class adapter) {
			if (IElementType.class.equals(adapter)) {
				return elementType;
			}
			return super.getAdapter(adapter);
		}
	}

}
