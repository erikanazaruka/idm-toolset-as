package tfm.diagram.part;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.update.DiagramUpdater;

import tfm.Actor;
import tfm.Cycle;
import tfm.FunctionalFeature;
import tfm.LogicalRelationship;
import tfm.TfmPackage;
import tfm.TopologicalRelationship;
import tfm.diagram.edit.parts.ActorEditPart;
import tfm.diagram.edit.parts.CycleEditPart;
import tfm.diagram.edit.parts.CycleFunctionalFeaturesEditPart;
import tfm.diagram.edit.parts.FunctionalFeatureEditPart;
import tfm.diagram.edit.parts.FunctionalFeatureEntityEditPart;
import tfm.diagram.edit.parts.LogicalRelationshipEditPart;
import tfm.diagram.edit.parts.LogicalRelationshipRelatedElementsEditPart;
import tfm.diagram.edit.parts.TFMEditPart;
import tfm.diagram.edit.parts.TopologicalRelationshipEditPart;
import tfm.diagram.providers.TFMElementTypes;

/**
 * @generated
 */
public class TFMDiagramUpdater {

	/**
	 * @generated
	 */
	public static List<TFMNodeDescriptor> getSemanticChildren(View view) {
		switch (TFMVisualIDRegistry.getVisualID(view)) {
		case TFMEditPart.VISUAL_ID:
			return getTFM_1000SemanticChildren(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TFMNodeDescriptor> getTFM_1000SemanticChildren(View view) {
		if (!view.isSetElement()) {
			return Collections.emptyList();
		}
		tfm.TFM modelElement = (tfm.TFM) view.getElement();
		LinkedList<TFMNodeDescriptor> result = new LinkedList<TFMNodeDescriptor>();
		for (Iterator<?> it = modelElement.getLogicalRelationships().iterator(); it
				.hasNext();) {
			LogicalRelationship childElement = (LogicalRelationship) it.next();
			int visualID = TFMVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == LogicalRelationshipEditPart.VISUAL_ID) {
				result.add(new TFMNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		for (Iterator<?> it = modelElement.getCycles().iterator(); it.hasNext();) {
			Cycle childElement = (Cycle) it.next();
			int visualID = TFMVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == CycleEditPart.VISUAL_ID) {
				result.add(new TFMNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		for (Iterator<?> it = modelElement.getActors().iterator(); it.hasNext();) {
			Actor childElement = (Actor) it.next();
			int visualID = TFMVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == ActorEditPart.VISUAL_ID) {
				result.add(new TFMNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		for (Iterator<?> it = modelElement.getFunctionalFeatures().iterator(); it
				.hasNext();) {
			FunctionalFeature childElement = (FunctionalFeature) it.next();
			int visualID = TFMVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == FunctionalFeatureEditPart.VISUAL_ID) {
				result.add(new TFMNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TFMLinkDescriptor> getContainedLinks(View view) {
		switch (TFMVisualIDRegistry.getVisualID(view)) {
		case TFMEditPart.VISUAL_ID:
			return getTFM_1000ContainedLinks(view);
		case LogicalRelationshipEditPart.VISUAL_ID:
			return getLogicalRelationship_2001ContainedLinks(view);
		case CycleEditPart.VISUAL_ID:
			return getCycle_2002ContainedLinks(view);
		case ActorEditPart.VISUAL_ID:
			return getActor_2003ContainedLinks(view);
		case FunctionalFeatureEditPart.VISUAL_ID:
			return getFunctionalFeature_2005ContainedLinks(view);
		case TopologicalRelationshipEditPart.VISUAL_ID:
			return getTopologicalRelationship_4007ContainedLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TFMLinkDescriptor> getIncomingLinks(View view) {
		switch (TFMVisualIDRegistry.getVisualID(view)) {
		case LogicalRelationshipEditPart.VISUAL_ID:
			return getLogicalRelationship_2001IncomingLinks(view);
		case CycleEditPart.VISUAL_ID:
			return getCycle_2002IncomingLinks(view);
		case ActorEditPart.VISUAL_ID:
			return getActor_2003IncomingLinks(view);
		case FunctionalFeatureEditPart.VISUAL_ID:
			return getFunctionalFeature_2005IncomingLinks(view);
		case TopologicalRelationshipEditPart.VISUAL_ID:
			return getTopologicalRelationship_4007IncomingLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TFMLinkDescriptor> getOutgoingLinks(View view) {
		switch (TFMVisualIDRegistry.getVisualID(view)) {
		case LogicalRelationshipEditPart.VISUAL_ID:
			return getLogicalRelationship_2001OutgoingLinks(view);
		case CycleEditPart.VISUAL_ID:
			return getCycle_2002OutgoingLinks(view);
		case ActorEditPart.VISUAL_ID:
			return getActor_2003OutgoingLinks(view);
		case FunctionalFeatureEditPart.VISUAL_ID:
			return getFunctionalFeature_2005OutgoingLinks(view);
		case TopologicalRelationshipEditPart.VISUAL_ID:
			return getTopologicalRelationship_4007OutgoingLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TFMLinkDescriptor> getTFM_1000ContainedLinks(View view) {
		tfm.TFM modelElement = (tfm.TFM) view.getElement();
		LinkedList<TFMLinkDescriptor> result = new LinkedList<TFMLinkDescriptor>();
		result.addAll(getContainedTypeModelFacetLinks_TopologicalRelationship_4007(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TFMLinkDescriptor> getLogicalRelationship_2001ContainedLinks(
			View view) {
		LogicalRelationship modelElement = (LogicalRelationship) view
				.getElement();
		LinkedList<TFMLinkDescriptor> result = new LinkedList<TFMLinkDescriptor>();
		result.addAll(getOutgoingFeatureModelFacetLinks_LogicalRelationship_RelatedElements_4009(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TFMLinkDescriptor> getCycle_2002ContainedLinks(View view) {
		Cycle modelElement = (Cycle) view.getElement();
		LinkedList<TFMLinkDescriptor> result = new LinkedList<TFMLinkDescriptor>();
		result.addAll(getOutgoingFeatureModelFacetLinks_Cycle_FunctionalFeatures_4005(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TFMLinkDescriptor> getActor_2003ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TFMLinkDescriptor> getFunctionalFeature_2005ContainedLinks(
			View view) {
		FunctionalFeature modelElement = (FunctionalFeature) view.getElement();
		LinkedList<TFMLinkDescriptor> result = new LinkedList<TFMLinkDescriptor>();
		result.addAll(getOutgoingFeatureModelFacetLinks_FunctionalFeature_Entity_4004(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TFMLinkDescriptor> getTopologicalRelationship_4007ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TFMLinkDescriptor> getLogicalRelationship_2001IncomingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TFMLinkDescriptor> getCycle_2002IncomingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TFMLinkDescriptor> getActor_2003IncomingLinks(View view) {
		Actor modelElement = (Actor) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<TFMLinkDescriptor> result = new LinkedList<TFMLinkDescriptor>();
		result.addAll(getIncomingFeatureModelFacetLinks_FunctionalFeature_Entity_4004(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TFMLinkDescriptor> getFunctionalFeature_2005IncomingLinks(
			View view) {
		FunctionalFeature modelElement = (FunctionalFeature) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<TFMLinkDescriptor> result = new LinkedList<TFMLinkDescriptor>();
		result.addAll(getIncomingFeatureModelFacetLinks_Cycle_FunctionalFeatures_4005(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_TopologicalRelationship_4007(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TFMLinkDescriptor> getTopologicalRelationship_4007IncomingLinks(
			View view) {
		TopologicalRelationship modelElement = (TopologicalRelationship) view
				.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<TFMLinkDescriptor> result = new LinkedList<TFMLinkDescriptor>();
		result.addAll(getIncomingFeatureModelFacetLinks_LogicalRelationship_RelatedElements_4009(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TFMLinkDescriptor> getLogicalRelationship_2001OutgoingLinks(
			View view) {
		LogicalRelationship modelElement = (LogicalRelationship) view
				.getElement();
		LinkedList<TFMLinkDescriptor> result = new LinkedList<TFMLinkDescriptor>();
		result.addAll(getOutgoingFeatureModelFacetLinks_LogicalRelationship_RelatedElements_4009(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TFMLinkDescriptor> getCycle_2002OutgoingLinks(View view) {
		Cycle modelElement = (Cycle) view.getElement();
		LinkedList<TFMLinkDescriptor> result = new LinkedList<TFMLinkDescriptor>();
		result.addAll(getOutgoingFeatureModelFacetLinks_Cycle_FunctionalFeatures_4005(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TFMLinkDescriptor> getActor_2003OutgoingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TFMLinkDescriptor> getFunctionalFeature_2005OutgoingLinks(
			View view) {
		FunctionalFeature modelElement = (FunctionalFeature) view.getElement();
		LinkedList<TFMLinkDescriptor> result = new LinkedList<TFMLinkDescriptor>();
		result.addAll(getOutgoingFeatureModelFacetLinks_FunctionalFeature_Entity_4004(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_TopologicalRelationship_4007(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TFMLinkDescriptor> getTopologicalRelationship_4007OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	private static Collection<TFMLinkDescriptor> getContainedTypeModelFacetLinks_TopologicalRelationship_4007(
			tfm.TFM container) {
		LinkedList<TFMLinkDescriptor> result = new LinkedList<TFMLinkDescriptor>();
		for (Iterator<?> links = container.getTopologicalRelationships()
				.iterator(); links.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof TopologicalRelationship) {
				continue;
			}
			TopologicalRelationship link = (TopologicalRelationship) linkObject;
			if (TopologicalRelationshipEditPart.VISUAL_ID != TFMVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			FunctionalFeature dst = link.getTarget();
			FunctionalFeature src = link.getSource();
			result.add(new TFMLinkDescriptor(src, dst, link,
					TFMElementTypes.TopologicalRelationship_4007,
					TopologicalRelationshipEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<TFMLinkDescriptor> getIncomingFeatureModelFacetLinks_LogicalRelationship_RelatedElements_4009(
			TopologicalRelationship target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<TFMLinkDescriptor> result = new LinkedList<TFMLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() == TfmPackage.eINSTANCE
					.getLogicalRelationship_RelatedElements()) {
				result.add(new TFMLinkDescriptor(
						setting.getEObject(),
						target,
						TFMElementTypes.LogicalRelationshipRelatedElements_4009,
						LogicalRelationshipRelatedElementsEditPart.VISUAL_ID));
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<TFMLinkDescriptor> getIncomingFeatureModelFacetLinks_FunctionalFeature_Entity_4004(
			Actor target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<TFMLinkDescriptor> result = new LinkedList<TFMLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() == TfmPackage.eINSTANCE
					.getFunctionalFeature_Entity()) {
				result.add(new TFMLinkDescriptor(setting.getEObject(), target,
						TFMElementTypes.FunctionalFeatureEntity_4004,
						FunctionalFeatureEntityEditPart.VISUAL_ID));
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<TFMLinkDescriptor> getIncomingFeatureModelFacetLinks_Cycle_FunctionalFeatures_4005(
			FunctionalFeature target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<TFMLinkDescriptor> result = new LinkedList<TFMLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() == TfmPackage.eINSTANCE
					.getCycle_FunctionalFeatures()) {
				result.add(new TFMLinkDescriptor(setting.getEObject(), target,
						TFMElementTypes.CycleFunctionalFeatures_4005,
						CycleFunctionalFeaturesEditPart.VISUAL_ID));
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<TFMLinkDescriptor> getIncomingTypeModelFacetLinks_TopologicalRelationship_4007(
			FunctionalFeature target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<TFMLinkDescriptor> result = new LinkedList<TFMLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() != TfmPackage.eINSTANCE
					.getTopologicalRelationship_Target()
					|| false == setting.getEObject() instanceof TopologicalRelationship) {
				continue;
			}
			TopologicalRelationship link = (TopologicalRelationship) setting
					.getEObject();
			if (TopologicalRelationshipEditPart.VISUAL_ID != TFMVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			FunctionalFeature src = link.getSource();
			result.add(new TFMLinkDescriptor(src, target, link,
					TFMElementTypes.TopologicalRelationship_4007,
					TopologicalRelationshipEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<TFMLinkDescriptor> getOutgoingFeatureModelFacetLinks_LogicalRelationship_RelatedElements_4009(
			LogicalRelationship source) {
		LinkedList<TFMLinkDescriptor> result = new LinkedList<TFMLinkDescriptor>();
		for (Iterator<?> destinations = source.getRelatedElements().iterator(); destinations
				.hasNext();) {
			TopologicalRelationship destination = (TopologicalRelationship) destinations
					.next();
			result.add(new TFMLinkDescriptor(source, destination,
					TFMElementTypes.LogicalRelationshipRelatedElements_4009,
					LogicalRelationshipRelatedElementsEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<TFMLinkDescriptor> getOutgoingFeatureModelFacetLinks_FunctionalFeature_Entity_4004(
			FunctionalFeature source) {
		LinkedList<TFMLinkDescriptor> result = new LinkedList<TFMLinkDescriptor>();
		Actor destination = source.getEntity();
		if (destination == null) {
			return result;
		}
		result.add(new TFMLinkDescriptor(source, destination,
				TFMElementTypes.FunctionalFeatureEntity_4004,
				FunctionalFeatureEntityEditPart.VISUAL_ID));
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<TFMLinkDescriptor> getOutgoingFeatureModelFacetLinks_Cycle_FunctionalFeatures_4005(
			Cycle source) {
		LinkedList<TFMLinkDescriptor> result = new LinkedList<TFMLinkDescriptor>();
		for (Iterator<?> destinations = source.getFunctionalFeatures()
				.iterator(); destinations.hasNext();) {
			FunctionalFeature destination = (FunctionalFeature) destinations
					.next();
			result.add(new TFMLinkDescriptor(source, destination,
					TFMElementTypes.CycleFunctionalFeatures_4005,
					CycleFunctionalFeaturesEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<TFMLinkDescriptor> getOutgoingTypeModelFacetLinks_TopologicalRelationship_4007(
			FunctionalFeature source) {
		tfm.TFM container = null;
		// Find container element for the link.
		// Climb up by containment hierarchy starting from the source
		// and return the first element that is instance of the container class.
		for (EObject element = source; element != null && container == null; element = element
				.eContainer()) {
			if (element instanceof tfm.TFM) {
				container = (tfm.TFM) element;
			}
		}
		if (container == null) {
			return Collections.emptyList();
		}
		LinkedList<TFMLinkDescriptor> result = new LinkedList<TFMLinkDescriptor>();
		for (Iterator<?> links = container.getTopologicalRelationships()
				.iterator(); links.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof TopologicalRelationship) {
				continue;
			}
			TopologicalRelationship link = (TopologicalRelationship) linkObject;
			if (TopologicalRelationshipEditPart.VISUAL_ID != TFMVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			FunctionalFeature dst = link.getTarget();
			FunctionalFeature src = link.getSource();
			if (src != source) {
				continue;
			}
			result.add(new TFMLinkDescriptor(src, dst, link,
					TFMElementTypes.TopologicalRelationship_4007,
					TopologicalRelationshipEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static final DiagramUpdater TYPED_INSTANCE = new DiagramUpdater() {
		/**
		 * @generated
		 */
		@Override
		public List<TFMNodeDescriptor> getSemanticChildren(View view) {
			return TFMDiagramUpdater.getSemanticChildren(view);
		}

		/**
		 * @generated
		 */
		@Override
		public List<TFMLinkDescriptor> getContainedLinks(View view) {
			return TFMDiagramUpdater.getContainedLinks(view);
		}

		/**
		 * @generated
		 */
		@Override
		public List<TFMLinkDescriptor> getIncomingLinks(View view) {
			return TFMDiagramUpdater.getIncomingLinks(view);
		}

		/**
		 * @generated
		 */
		@Override
		public List<TFMLinkDescriptor> getOutgoingLinks(View view) {
			return TFMDiagramUpdater.getOutgoingLinks(view);
		}
	};

}
