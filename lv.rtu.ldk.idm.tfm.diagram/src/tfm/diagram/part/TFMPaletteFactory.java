package tfm.diagram.part;

import java.util.Collections;
import java.util.List;

import org.eclipse.gef.Tool;
import org.eclipse.gef.palette.PaletteContainer;
import org.eclipse.gef.palette.PaletteGroup;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.palette.ToolEntry;
import org.eclipse.gmf.runtime.diagram.ui.tools.UnspecifiedTypeConnectionTool;
import org.eclipse.gmf.runtime.diagram.ui.tools.UnspecifiedTypeCreationTool;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;

import tfm.diagram.providers.TFMElementTypes;

/**
 * @generated
 */
public class TFMPaletteFactory {

	/**
	 * @generated
	 */
	public void fillPalette(PaletteRoot paletteRoot) {
		paletteRoot.add(createTfm1Group());
	}

	/**
	 * Creates "tfm" palette tool group
	 * @generated
	 */
	private PaletteContainer createTfm1Group() {
		PaletteGroup paletteContainer = new PaletteGroup(
				Messages.Tfm1Group_title);
		paletteContainer.setId("createTfm1Group"); //$NON-NLS-1$
		paletteContainer.add(createActor1CreationTool());
		paletteContainer.add(createFunctionalFeature2CreationTool());
		paletteContainer.add(createTopologicalRelationship3CreationTool());
		paletteContainer.add(createCycle4CreationTool());
		paletteContainer.add(createLogicalRelationship5CreationTool());
		paletteContainer.add(createLinkLogicalRelationship6CreationTool());
		return paletteContainer;
	}

	/**
	 * @generated
	 */
	private ToolEntry createActor1CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.Actor1CreationTool_title,
				Messages.Actor1CreationTool_desc,
				Collections.singletonList(TFMElementTypes.Actor_2003));
		entry.setId("createActor1CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TFMElementTypes
				.getImageDescriptor(TFMElementTypes.Actor_2003));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createFunctionalFeature2CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.FunctionalFeature2CreationTool_title,
				Messages.FunctionalFeature2CreationTool_desc,
				Collections
						.singletonList(TFMElementTypes.FunctionalFeature_2005));
		entry.setId("createFunctionalFeature2CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TFMElementTypes
				.getImageDescriptor(TFMElementTypes.FunctionalFeature_2005));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createTopologicalRelationship3CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				Messages.TopologicalRelationship3CreationTool_title,
				Messages.TopologicalRelationship3CreationTool_desc,
				Collections
						.singletonList(TFMElementTypes.TopologicalRelationship_4007));
		entry.setId("createTopologicalRelationship3CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TFMDiagramEditorPlugin
				.findImageDescriptor("/lv.rtu.ldk.idm.tfm.edit/icons/full/obj16/TopologicalRelationship.gif")); //$NON-NLS-1$
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createCycle4CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.Cycle4CreationTool_title,
				Messages.Cycle4CreationTool_desc,
				Collections.singletonList(TFMElementTypes.Cycle_2002));
		entry.setId("createCycle4CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TFMElementTypes
				.getImageDescriptor(TFMElementTypes.Cycle_2002));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createLogicalRelationship5CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.LogicalRelationship5CreationTool_title,
				Messages.LogicalRelationship5CreationTool_desc,
				Collections
						.singletonList(TFMElementTypes.LogicalRelationship_2001));
		entry.setId("createLogicalRelationship5CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TFMElementTypes
				.getImageDescriptor(TFMElementTypes.LogicalRelationship_2001));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createLinkLogicalRelationship6CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				Messages.LinkLogicalRelationship6CreationTool_title,
				Messages.LinkLogicalRelationship6CreationTool_desc,
				Collections
						.singletonList(TFMElementTypes.LogicalRelationshipRelatedElements_4009));
		entry.setId("createLinkLogicalRelationship6CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TFMElementTypes
				.getImageDescriptor(TFMElementTypes.LogicalRelationshipRelatedElements_4009));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private static class NodeToolEntry extends ToolEntry {

		/**
		 * @generated
		 */
		private final List<IElementType> elementTypes;

		/**
		 * @generated
		 */
		private NodeToolEntry(String title, String description,
				List<IElementType> elementTypes) {
			super(title, description, null, null);
			this.elementTypes = elementTypes;
		}

		/**
		 * @generated
		 */
		public Tool createTool() {
			Tool tool = new UnspecifiedTypeCreationTool(elementTypes);
			tool.setProperties(getToolProperties());
			return tool;
		}
	}

	/**
	 * @generated
	 */
	private static class LinkToolEntry extends ToolEntry {

		/**
		 * @generated
		 */
		private final List<IElementType> relationshipTypes;

		/**
		 * @generated
		 */
		private LinkToolEntry(String title, String description,
				List<IElementType> relationshipTypes) {
			super(title, description, null, null);
			this.relationshipTypes = relationshipTypes;
		}

		/**
		 * @generated
		 */
		public Tool createTool() {
			Tool tool = new UnspecifiedTypeConnectionTool(relationshipTypes);
			tool.setProperties(getToolProperties());
			return tool;
		}
	}
}
