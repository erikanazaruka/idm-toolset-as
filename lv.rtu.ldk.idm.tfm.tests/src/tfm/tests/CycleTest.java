/**
 */
package tfm.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import tfm.Cycle;
import tfm.TfmFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Cycle</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class CycleTest extends TestCase {

	/**
	 * The fixture for this Cycle test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Cycle fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(CycleTest.class);
	}

	/**
	 * Constructs a new Cycle test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CycleTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Cycle test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(Cycle fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Cycle test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Cycle getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(TfmFactory.eINSTANCE.createCycle());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //CycleTest
