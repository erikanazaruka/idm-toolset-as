/**
 */
package tfm.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import tfm.TfmFactory;
import tfm.TopologicalRelationship;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Topological Relationship</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class TopologicalRelationshipTest extends TestCase {

	/**
	 * The fixture for this Topological Relationship test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TopologicalRelationship fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(TopologicalRelationshipTest.class);
	}

	/**
	 * Constructs a new Topological Relationship test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TopologicalRelationshipTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Topological Relationship test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(TopologicalRelationship fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Topological Relationship test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TopologicalRelationship getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(TfmFactory.eINSTANCE.createTopologicalRelationship());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //TopologicalRelationshipTest
