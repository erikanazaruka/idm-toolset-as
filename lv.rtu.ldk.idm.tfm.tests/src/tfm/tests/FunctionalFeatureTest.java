/**
 */
package tfm.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import tfm.FunctionalFeature;
import tfm.TfmFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Functional Feature</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class FunctionalFeatureTest extends TestCase {

	/**
	 * The fixture for this Functional Feature test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FunctionalFeature fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(FunctionalFeatureTest.class);
	}

	/**
	 * Constructs a new Functional Feature test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionalFeatureTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Functional Feature test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(FunctionalFeature fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Functional Feature test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FunctionalFeature getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(TfmFactory.eINSTANCE.createFunctionalFeature());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //FunctionalFeatureTest
