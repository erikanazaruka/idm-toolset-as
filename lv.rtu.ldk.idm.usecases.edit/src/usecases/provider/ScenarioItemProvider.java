package usecases.provider;
/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */



import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;

import usecases.AlternativeScenario;
import usecases.Extension;
import usecases.MainScenario;
import usecases.SubVariation;

/**
 * This is the item provider adapter for a {@link usecases.Scenario} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ScenarioItemProvider
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScenarioItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String getText(Object object) {
		//return getString("_UI_Scenario_type");
		
		String type = "";
		if(object instanceof MainScenario){
			type = getString("_UI_MainScenario_type");
		} else if(object instanceof Extension){
			type = getString("_UI_Extension_type");
		} else if(object instanceof SubVariation){
			type = getString("_UI_SubVariation_type");
		}
		
		// No more precodnitions for scenatios because that would cause confusion
		// from now on preconditons must be defined in the events
		
//		Scenario scenario = (Scenario) object;
//		EList<Event> events = new BasicEList<Event>();
//		
//		//This is done  because previously multiple events were allowed
//		//now instead you would use a CompositeEvent
//		events.add(scenario.getPreconditions());
//
//		String preconditionString = "";
//		if(events!=null){
//			Iterator<Event> it = events.iterator();
//			while(it.hasNext()) {
//			  Object event = it.next();
//			  if(event instanceof SingleEvent){
//				  SingleEvent singleEvent = (SingleEvent) event;
//				  preconditionString = preconditionString + singleEvent.getDescription();
//			  } else if(event instanceof CompositeEvent){
//				  CompositeEvent compositeEvent = (CompositeEvent) event;
//				  EList<Event> cEvents = compositeEvent.getEvents();
//				  
//				  Iterator<Event> cIt = cEvents.iterator();
//				  while(cIt.hasNext()) {
//						Object cEvent = cIt.next();
//						if(cEvent instanceof SingleEvent){
//							preconditionString = preconditionString + ((SingleEvent)cEvent).getDescription();
//						} else if(cEvent instanceof CompositeEvent){
//							preconditionString = preconditionString + "(...)";
//						}
//						if(cIt.hasNext()){
//							preconditionString = preconditionString + " " + compositeEvent.getOperation().getName() + " ";
//						}
//				  }
//				  
//			  }
//			  if(it.hasNext()){
//				  preconditionString = preconditionString + ", ";
//			  }
//			  
//			}
//		}

		String reference = "";
		if(object instanceof AlternativeScenario){
			AlternativeScenario alternativeScenario = (AlternativeScenario) object;
			if(alternativeScenario.getReference()!=null){
				reference = String.valueOf(alternativeScenario.getReference().getId());
				reference = " for " + reference + " (" + alternativeScenario.getId() + ")";
			}	
		}
		
		
		//return  type + reference + ": [" + preconditionString + "]";
		return  type + reference + ":";
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return UseCasesEditPlugin.INSTANCE;
	}

}
