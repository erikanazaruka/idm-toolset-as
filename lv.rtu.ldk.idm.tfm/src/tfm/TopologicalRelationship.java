/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package tfm;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Topological Relationship</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link tfm.TopologicalRelationship#getId <em>Id</em>}</li>
 *   <li>{@link tfm.TopologicalRelationship#getSource <em>Source</em>}</li>
 *   <li>{@link tfm.TopologicalRelationship#getTarget <em>Target</em>}</li>
 * </ul>
 * </p>
 *
 * @see tfm.TfmPackage#getTopologicalRelationship()
 * @model
 * @generated
 */
public interface TopologicalRelationship extends EObject {
	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see tfm.TfmPackage#getTopologicalRelationship_Id()
	 * @model required="true"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link tfm.TopologicalRelationship#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' reference.
	 * @see #setSource(FunctionalFeature)
	 * @see tfm.TfmPackage#getTopologicalRelationship_Source()
	 * @model required="true"
	 * @generated
	 */
	FunctionalFeature getSource();

	/**
	 * Sets the value of the '{@link tfm.TopologicalRelationship#getSource <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(FunctionalFeature value);

	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(FunctionalFeature)
	 * @see tfm.TfmPackage#getTopologicalRelationship_Target()
	 * @model required="true"
	 * @generated
	 */
	FunctionalFeature getTarget();

	/**
	 * Sets the value of the '{@link tfm.TopologicalRelationship#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(FunctionalFeature value);

} // TopologicalRelationship
