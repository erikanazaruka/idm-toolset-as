/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package tfm;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Cycle</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link tfm.Cycle#getOrder <em>Order</em>}</li>
 *   <li>{@link tfm.Cycle#isIsMain <em>Is Main</em>}</li>
 *   <li>{@link tfm.Cycle#getFunctionalFeatures <em>Functional Features</em>}</li>
 * </ul>
 * </p>
 *
 * @see tfm.TfmPackage#getCycle()
 * @model
 * @generated
 */
public interface Cycle extends EObject {
	/**
	 * Returns the value of the '<em><b>Order</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Order</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Order</em>' attribute.
	 * @see #setOrder(int)
	 * @see tfm.TfmPackage#getCycle_Order()
	 * @model required="true"
	 * @generated
	 */
	int getOrder();

	/**
	 * Sets the value of the '{@link tfm.Cycle#getOrder <em>Order</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Order</em>' attribute.
	 * @see #getOrder()
	 * @generated
	 */
	void setOrder(int value);

	/**
	 * Returns the value of the '<em><b>Is Main</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Main</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Main</em>' attribute.
	 * @see #setIsMain(boolean)
	 * @see tfm.TfmPackage#getCycle_IsMain()
	 * @model required="true"
	 * @generated
	 */
	boolean isIsMain();

	/**
	 * Sets the value of the '{@link tfm.Cycle#isIsMain <em>Is Main</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Main</em>' attribute.
	 * @see #isIsMain()
	 * @generated
	 */
	void setIsMain(boolean value);

	/**
	 * Returns the value of the '<em><b>Functional Features</b></em>' reference list.
	 * The list contents are of type {@link tfm.FunctionalFeature}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Functional Features</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Functional Features</em>' reference list.
	 * @see tfm.TfmPackage#getCycle_FunctionalFeatures()
	 * @model lower="2"
	 * @generated
	 */
	EList<FunctionalFeature> getFunctionalFeatures();

} // Cycle
