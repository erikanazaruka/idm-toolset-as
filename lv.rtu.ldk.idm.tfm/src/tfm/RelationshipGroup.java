/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package tfm;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Relationship Group</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link tfm.RelationshipGroup#getRelationships <em>Relationships</em>}</li>
 *   <li>{@link tfm.RelationshipGroup#getId <em>Id</em>}</li>
 *   <li>{@link tfm.RelationshipGroup#getOperation <em>Operation</em>}</li>
 * </ul>
 * </p>
 *
 * @see tfm.TfmPackage#getRelationshipGroup()
 * @model
 * @generated
 */
public interface RelationshipGroup extends EObject {
	/**
	 * Returns the value of the '<em><b>Relationships</b></em>' reference list.
	 * The list contents are of type {@link tfm.TopologicalRelationship}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Relationships</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relationships</em>' reference list.
	 * @see tfm.TfmPackage#getRelationshipGroup_Relationships()
	 * @model required="true"
	 * @generated
	 */
	EList<TopologicalRelationship> getRelationships();

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(int)
	 * @see tfm.TfmPackage#getRelationshipGroup_Id()
	 * @model
	 * @generated
	 */
	int getId();

	/**
	 * Sets the value of the '{@link tfm.RelationshipGroup#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(int value);

	/**
	 * Returns the value of the '<em><b>Operation</b></em>' attribute.
	 * The literals are from the enumeration {@link tfm.LogicalOperation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operation</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operation</em>' attribute.
	 * @see tfm.LogicalOperation
	 * @see #setOperation(LogicalOperation)
	 * @see tfm.TfmPackage#getRelationshipGroup_Operation()
	 * @model
	 * @generated
	 */
	LogicalOperation getOperation();

	/**
	 * Sets the value of the '{@link tfm.RelationshipGroup#getOperation <em>Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operation</em>' attribute.
	 * @see tfm.LogicalOperation
	 * @see #getOperation()
	 * @generated
	 */
	void setOperation(LogicalOperation value);

} // RelationshipGroup
