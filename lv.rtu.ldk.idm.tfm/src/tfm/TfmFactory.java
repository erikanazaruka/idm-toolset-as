/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package tfm;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see tfm.TfmPackage
 * @generated
 */
public interface TfmFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TfmFactory eINSTANCE = tfm.impl.TfmFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>TFM</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>TFM</em>'.
	 * @generated
	 */
	TFM createTFM();

	/**
	 * Returns a new object of class '<em>Functional Feature</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Functional Feature</em>'.
	 * @generated
	 */
	FunctionalFeature createFunctionalFeature();

	/**
	 * Returns a new object of class '<em>Cycle</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cycle</em>'.
	 * @generated
	 */
	Cycle createCycle();

	/**
	 * Returns a new object of class '<em>Actor</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Actor</em>'.
	 * @generated
	 */
	Actor createActor();

	/**
	 * Returns a new object of class '<em>Topological Relationship</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Topological Relationship</em>'.
	 * @generated
	 */
	TopologicalRelationship createTopologicalRelationship();

	/**
	 * Returns a new object of class '<em>Logical Relationship</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Logical Relationship</em>'.
	 * @generated
	 */
	LogicalRelationship createLogicalRelationship();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	TfmPackage getTfmPackage();

} //TfmFactory
