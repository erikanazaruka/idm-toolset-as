/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package tfm.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;

import tfm.Actor;
import tfm.FunctionalFeature;
import tfm.Subordination;
import tfm.TfmPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Functional Feature</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link tfm.impl.FunctionalFeatureImpl#getId <em>Id</em>}</li>
 *   <li>{@link tfm.impl.FunctionalFeatureImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link tfm.impl.FunctionalFeatureImpl#getAction <em>Action</em>}</li>
 *   <li>{@link tfm.impl.FunctionalFeatureImpl#getResult <em>Result</em>}</li>
 *   <li>{@link tfm.impl.FunctionalFeatureImpl#getObject <em>Object</em>}</li>
 *   <li>{@link tfm.impl.FunctionalFeatureImpl#getEntity <em>Entity</em>}</li>
 *   <li>{@link tfm.impl.FunctionalFeatureImpl#getPrecond <em>Precond</em>}</li>
 *   <li>{@link tfm.impl.FunctionalFeatureImpl#getPostcond <em>Postcond</em>}</li>
 *   <li>{@link tfm.impl.FunctionalFeatureImpl#getSubordination <em>Subordination</em>}</li>
 *   <li>{@link tfm.impl.FunctionalFeatureImpl#isExecutorIsSystem <em>Executor Is System</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class FunctionalFeatureImpl extends EObjectImpl implements FunctionalFeature {
	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getAction() <em>Action</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAction()
	 * @generated
	 * @ordered
	 */
	protected static final String ACTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAction() <em>Action</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAction()
	 * @generated
	 * @ordered
	 */
	protected String action = ACTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getResult() <em>Result</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResult()
	 * @generated
	 * @ordered
	 */
	protected static final String RESULT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getResult() <em>Result</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResult()
	 * @generated
	 * @ordered
	 */
	protected String result = RESULT_EDEFAULT;

	/**
	 * The default value of the '{@link #getObject() <em>Object</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObject()
	 * @generated
	 * @ordered
	 */
	protected static final String OBJECT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getObject() <em>Object</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObject()
	 * @generated
	 * @ordered
	 */
	protected String object = OBJECT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getEntity() <em>Entity</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEntity()
	 * @generated
	 * @ordered
	 */
	protected Actor entity;

	/**
	 * The default value of the '{@link #getPrecond() <em>Precond</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrecond()
	 * @generated
	 * @ordered
	 */
	protected static final String PRECOND_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPrecond() <em>Precond</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrecond()
	 * @generated
	 * @ordered
	 */
	protected String precond = PRECOND_EDEFAULT;

	/**
	 * The default value of the '{@link #getPostcond() <em>Postcond</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPostcond()
	 * @generated
	 * @ordered
	 */
	protected static final String POSTCOND_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPostcond() <em>Postcond</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPostcond()
	 * @generated
	 * @ordered
	 */
	protected String postcond = POSTCOND_EDEFAULT;

	/**
	 * The default value of the '{@link #getSubordination() <em>Subordination</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubordination()
	 * @generated
	 * @ordered
	 */
	protected static final Subordination SUBORDINATION_EDEFAULT = Subordination.INNER;

	/**
	 * The cached value of the '{@link #getSubordination() <em>Subordination</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubordination()
	 * @generated
	 * @ordered
	 */
	protected Subordination subordination = SUBORDINATION_EDEFAULT;

	/**
	 * The default value of the '{@link #isExecutorIsSystem() <em>Executor Is System</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isExecutorIsSystem()
	 * @generated
	 * @ordered
	 */
	protected static final boolean EXECUTOR_IS_SYSTEM_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isExecutorIsSystem() <em>Executor Is System</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isExecutorIsSystem()
	 * @generated
	 * @ordered
	 */
	protected boolean executorIsSystem = EXECUTOR_IS_SYSTEM_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FunctionalFeatureImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TfmPackage.Literals.FUNCTIONAL_FEATURE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TfmPackage.FUNCTIONAL_FEATURE__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TfmPackage.FUNCTIONAL_FEATURE__DESCRIPTION, oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAction() {
		return action;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAction(String newAction) {
		String oldAction = action;
		action = newAction;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TfmPackage.FUNCTIONAL_FEATURE__ACTION, oldAction, action));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getResult() {
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResult(String newResult) {
		String oldResult = result;
		result = newResult;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TfmPackage.FUNCTIONAL_FEATURE__RESULT, oldResult, result));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getObject() {
		return object;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObject(String newObject) {
		String oldObject = object;
		object = newObject;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TfmPackage.FUNCTIONAL_FEATURE__OBJECT, oldObject, object));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Actor getEntity() {
		if (entity != null && entity.eIsProxy()) {
			InternalEObject oldEntity = (InternalEObject)entity;
			entity = (Actor)eResolveProxy(oldEntity);
			if (entity != oldEntity) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TfmPackage.FUNCTIONAL_FEATURE__ENTITY, oldEntity, entity));
			}
		}
		return entity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Actor basicGetEntity() {
		return entity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEntity(Actor newEntity) {
		Actor oldEntity = entity;
		entity = newEntity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TfmPackage.FUNCTIONAL_FEATURE__ENTITY, oldEntity, entity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPrecond() {
		return precond;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPrecond(String newPrecond) {
		String oldPrecond = precond;
		precond = newPrecond;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TfmPackage.FUNCTIONAL_FEATURE__PRECOND, oldPrecond, precond));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPostcond() {
		return postcond;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPostcond(String newPostcond) {
		String oldPostcond = postcond;
		postcond = newPostcond;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TfmPackage.FUNCTIONAL_FEATURE__POSTCOND, oldPostcond, postcond));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Subordination getSubordination() {
		return subordination;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSubordination(Subordination newSubordination) {
		Subordination oldSubordination = subordination;
		subordination = newSubordination == null ? SUBORDINATION_EDEFAULT : newSubordination;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TfmPackage.FUNCTIONAL_FEATURE__SUBORDINATION, oldSubordination, subordination));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isExecutorIsSystem() {
		return executorIsSystem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExecutorIsSystem(boolean newExecutorIsSystem) {
		boolean oldExecutorIsSystem = executorIsSystem;
		executorIsSystem = newExecutorIsSystem;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TfmPackage.FUNCTIONAL_FEATURE__EXECUTOR_IS_SYSTEM, oldExecutorIsSystem, executorIsSystem));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TfmPackage.FUNCTIONAL_FEATURE__ID:
				return getId();
			case TfmPackage.FUNCTIONAL_FEATURE__DESCRIPTION:
				return getDescription();
			case TfmPackage.FUNCTIONAL_FEATURE__ACTION:
				return getAction();
			case TfmPackage.FUNCTIONAL_FEATURE__RESULT:
				return getResult();
			case TfmPackage.FUNCTIONAL_FEATURE__OBJECT:
				return getObject();
			case TfmPackage.FUNCTIONAL_FEATURE__ENTITY:
				if (resolve) return getEntity();
				return basicGetEntity();
			case TfmPackage.FUNCTIONAL_FEATURE__PRECOND:
				return getPrecond();
			case TfmPackage.FUNCTIONAL_FEATURE__POSTCOND:
				return getPostcond();
			case TfmPackage.FUNCTIONAL_FEATURE__SUBORDINATION:
				return getSubordination();
			case TfmPackage.FUNCTIONAL_FEATURE__EXECUTOR_IS_SYSTEM:
				return isExecutorIsSystem();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TfmPackage.FUNCTIONAL_FEATURE__ID:
				setId((String)newValue);
				return;
			case TfmPackage.FUNCTIONAL_FEATURE__DESCRIPTION:
				setDescription((String)newValue);
				return;
			case TfmPackage.FUNCTIONAL_FEATURE__ACTION:
				setAction((String)newValue);
				return;
			case TfmPackage.FUNCTIONAL_FEATURE__RESULT:
				setResult((String)newValue);
				return;
			case TfmPackage.FUNCTIONAL_FEATURE__OBJECT:
				setObject((String)newValue);
				return;
			case TfmPackage.FUNCTIONAL_FEATURE__ENTITY:
				setEntity((Actor)newValue);
				return;
			case TfmPackage.FUNCTIONAL_FEATURE__PRECOND:
				setPrecond((String)newValue);
				return;
			case TfmPackage.FUNCTIONAL_FEATURE__POSTCOND:
				setPostcond((String)newValue);
				return;
			case TfmPackage.FUNCTIONAL_FEATURE__SUBORDINATION:
				setSubordination((Subordination)newValue);
				return;
			case TfmPackage.FUNCTIONAL_FEATURE__EXECUTOR_IS_SYSTEM:
				setExecutorIsSystem((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TfmPackage.FUNCTIONAL_FEATURE__ID:
				setId(ID_EDEFAULT);
				return;
			case TfmPackage.FUNCTIONAL_FEATURE__DESCRIPTION:
				setDescription(DESCRIPTION_EDEFAULT);
				return;
			case TfmPackage.FUNCTIONAL_FEATURE__ACTION:
				setAction(ACTION_EDEFAULT);
				return;
			case TfmPackage.FUNCTIONAL_FEATURE__RESULT:
				setResult(RESULT_EDEFAULT);
				return;
			case TfmPackage.FUNCTIONAL_FEATURE__OBJECT:
				setObject(OBJECT_EDEFAULT);
				return;
			case TfmPackage.FUNCTIONAL_FEATURE__ENTITY:
				setEntity((Actor)null);
				return;
			case TfmPackage.FUNCTIONAL_FEATURE__PRECOND:
				setPrecond(PRECOND_EDEFAULT);
				return;
			case TfmPackage.FUNCTIONAL_FEATURE__POSTCOND:
				setPostcond(POSTCOND_EDEFAULT);
				return;
			case TfmPackage.FUNCTIONAL_FEATURE__SUBORDINATION:
				setSubordination(SUBORDINATION_EDEFAULT);
				return;
			case TfmPackage.FUNCTIONAL_FEATURE__EXECUTOR_IS_SYSTEM:
				setExecutorIsSystem(EXECUTOR_IS_SYSTEM_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TfmPackage.FUNCTIONAL_FEATURE__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case TfmPackage.FUNCTIONAL_FEATURE__DESCRIPTION:
				return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
			case TfmPackage.FUNCTIONAL_FEATURE__ACTION:
				return ACTION_EDEFAULT == null ? action != null : !ACTION_EDEFAULT.equals(action);
			case TfmPackage.FUNCTIONAL_FEATURE__RESULT:
				return RESULT_EDEFAULT == null ? result != null : !RESULT_EDEFAULT.equals(result);
			case TfmPackage.FUNCTIONAL_FEATURE__OBJECT:
				return OBJECT_EDEFAULT == null ? object != null : !OBJECT_EDEFAULT.equals(object);
			case TfmPackage.FUNCTIONAL_FEATURE__ENTITY:
				return entity != null;
			case TfmPackage.FUNCTIONAL_FEATURE__PRECOND:
				return PRECOND_EDEFAULT == null ? precond != null : !PRECOND_EDEFAULT.equals(precond);
			case TfmPackage.FUNCTIONAL_FEATURE__POSTCOND:
				return POSTCOND_EDEFAULT == null ? postcond != null : !POSTCOND_EDEFAULT.equals(postcond);
			case TfmPackage.FUNCTIONAL_FEATURE__SUBORDINATION:
				return subordination != SUBORDINATION_EDEFAULT;
			case TfmPackage.FUNCTIONAL_FEATURE__EXECUTOR_IS_SYSTEM:
				return executorIsSystem != EXECUTOR_IS_SYSTEM_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", description: ");
		result.append(description);
		result.append(", action: ");
		result.append(action);
		result.append(", result: ");
		result.append(result);
		result.append(", object: ");
		result.append(object);
		result.append(", precond: ");
		result.append(precond);
		result.append(", postcond: ");
		result.append(postcond);
		result.append(", subordination: ");
		result.append(subordination);
		result.append(", executorIsSystem: ");
		result.append(executorIsSystem);
		result.append(')');
		return result.toString();
	}

} //FunctionalFeatureImpl
