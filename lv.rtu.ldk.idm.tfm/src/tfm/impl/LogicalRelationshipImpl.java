/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package tfm.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import tfm.LogicalOperation;
import tfm.LogicalRelationship;
import tfm.TfmPackage;
import tfm.TopologicalRelationship;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Logical Relationship</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link tfm.impl.LogicalRelationshipImpl#getId <em>Id</em>}</li>
 *   <li>{@link tfm.impl.LogicalRelationshipImpl#getOperation <em>Operation</em>}</li>
 *   <li>{@link tfm.impl.LogicalRelationshipImpl#getRelatedElements <em>Related Elements</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class LogicalRelationshipImpl extends EObjectImpl implements LogicalRelationship {
	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getOperation() <em>Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperation()
	 * @generated
	 * @ordered
	 */
	protected static final LogicalOperation OPERATION_EDEFAULT = LogicalOperation.AND;

	/**
	 * The cached value of the '{@link #getOperation() <em>Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperation()
	 * @generated
	 * @ordered
	 */
	protected LogicalOperation operation = OPERATION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRelatedElements() <em>Related Elements</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRelatedElements()
	 * @generated
	 * @ordered
	 */
	protected EList<TopologicalRelationship> relatedElements;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LogicalRelationshipImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TfmPackage.Literals.LOGICAL_RELATIONSHIP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TfmPackage.LOGICAL_RELATIONSHIP__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LogicalOperation getOperation() {
		return operation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperation(LogicalOperation newOperation) {
		LogicalOperation oldOperation = operation;
		operation = newOperation == null ? OPERATION_EDEFAULT : newOperation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TfmPackage.LOGICAL_RELATIONSHIP__OPERATION, oldOperation, operation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TopologicalRelationship> getRelatedElements() {
		if (relatedElements == null) {
			relatedElements = new EObjectResolvingEList<TopologicalRelationship>(TopologicalRelationship.class, this, TfmPackage.LOGICAL_RELATIONSHIP__RELATED_ELEMENTS);
		}
		return relatedElements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TfmPackage.LOGICAL_RELATIONSHIP__ID:
				return getId();
			case TfmPackage.LOGICAL_RELATIONSHIP__OPERATION:
				return getOperation();
			case TfmPackage.LOGICAL_RELATIONSHIP__RELATED_ELEMENTS:
				return getRelatedElements();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TfmPackage.LOGICAL_RELATIONSHIP__ID:
				setId((String)newValue);
				return;
			case TfmPackage.LOGICAL_RELATIONSHIP__OPERATION:
				setOperation((LogicalOperation)newValue);
				return;
			case TfmPackage.LOGICAL_RELATIONSHIP__RELATED_ELEMENTS:
				getRelatedElements().clear();
				getRelatedElements().addAll((Collection<? extends TopologicalRelationship>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TfmPackage.LOGICAL_RELATIONSHIP__ID:
				setId(ID_EDEFAULT);
				return;
			case TfmPackage.LOGICAL_RELATIONSHIP__OPERATION:
				setOperation(OPERATION_EDEFAULT);
				return;
			case TfmPackage.LOGICAL_RELATIONSHIP__RELATED_ELEMENTS:
				getRelatedElements().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TfmPackage.LOGICAL_RELATIONSHIP__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case TfmPackage.LOGICAL_RELATIONSHIP__OPERATION:
				return operation != OPERATION_EDEFAULT;
			case TfmPackage.LOGICAL_RELATIONSHIP__RELATED_ELEMENTS:
				return relatedElements != null && !relatedElements.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", operation: ");
		result.append(operation);
		result.append(')');
		return result.toString();
	}

} //LogicalRelationshipImpl
