/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package tfm.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import tfm.Actor;
import tfm.Cycle;
import tfm.FunctionalFeature;
import tfm.LogicalRelationship;
import tfm.TFM;
import tfm.TfmPackage;
import tfm.TopologicalRelationship;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>TFM</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link tfm.impl.TFMImpl#getFunctionalFeatures <em>Functional Features</em>}</li>
 *   <li>{@link tfm.impl.TFMImpl#getTopologicalRelationships <em>Topological Relationships</em>}</li>
 *   <li>{@link tfm.impl.TFMImpl#getLogicalRelationships <em>Logical Relationships</em>}</li>
 *   <li>{@link tfm.impl.TFMImpl#getActors <em>Actors</em>}</li>
 *   <li>{@link tfm.impl.TFMImpl#getCycles <em>Cycles</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class TFMImpl extends EObjectImpl implements TFM {
	/**
	 * The cached value of the '{@link #getFunctionalFeatures() <em>Functional Features</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFunctionalFeatures()
	 * @generated
	 * @ordered
	 */
	protected EList<FunctionalFeature> functionalFeatures;

	/**
	 * The cached value of the '{@link #getTopologicalRelationships() <em>Topological Relationships</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTopologicalRelationships()
	 * @generated
	 * @ordered
	 */
	protected EList<TopologicalRelationship> topologicalRelationships;

	/**
	 * The cached value of the '{@link #getLogicalRelationships() <em>Logical Relationships</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLogicalRelationships()
	 * @generated
	 * @ordered
	 */
	protected EList<LogicalRelationship> logicalRelationships;

	/**
	 * The cached value of the '{@link #getActors() <em>Actors</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActors()
	 * @generated
	 * @ordered
	 */
	protected EList<Actor> actors;

	/**
	 * The cached value of the '{@link #getCycles() <em>Cycles</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCycles()
	 * @generated
	 * @ordered
	 */
	protected EList<Cycle> cycles;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TFMImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TfmPackage.Literals.TFM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TopologicalRelationship> getTopologicalRelationships() {
		if (topologicalRelationships == null) {
			topologicalRelationships = new EObjectContainmentEList<TopologicalRelationship>(TopologicalRelationship.class, this, TfmPackage.TFM__TOPOLOGICAL_RELATIONSHIPS);
		}
		return topologicalRelationships;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LogicalRelationship> getLogicalRelationships() {
		if (logicalRelationships == null) {
			logicalRelationships = new EObjectContainmentEList<LogicalRelationship>(LogicalRelationship.class, this, TfmPackage.TFM__LOGICAL_RELATIONSHIPS);
		}
		return logicalRelationships;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Actor> getActors() {
		if (actors == null) {
			actors = new EObjectContainmentEList<Actor>(Actor.class, this, TfmPackage.TFM__ACTORS);
		}
		return actors;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Cycle> getCycles() {
		if (cycles == null) {
			cycles = new EObjectContainmentEList<Cycle>(Cycle.class, this, TfmPackage.TFM__CYCLES);
		}
		return cycles;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FunctionalFeature> getFunctionalFeatures() {
		if (functionalFeatures == null) {
			functionalFeatures = new EObjectContainmentEList<FunctionalFeature>(FunctionalFeature.class, this, TfmPackage.TFM__FUNCTIONAL_FEATURES);
		}
		return functionalFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TfmPackage.TFM__FUNCTIONAL_FEATURES:
				return ((InternalEList<?>)getFunctionalFeatures()).basicRemove(otherEnd, msgs);
			case TfmPackage.TFM__TOPOLOGICAL_RELATIONSHIPS:
				return ((InternalEList<?>)getTopologicalRelationships()).basicRemove(otherEnd, msgs);
			case TfmPackage.TFM__LOGICAL_RELATIONSHIPS:
				return ((InternalEList<?>)getLogicalRelationships()).basicRemove(otherEnd, msgs);
			case TfmPackage.TFM__ACTORS:
				return ((InternalEList<?>)getActors()).basicRemove(otherEnd, msgs);
			case TfmPackage.TFM__CYCLES:
				return ((InternalEList<?>)getCycles()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TfmPackage.TFM__FUNCTIONAL_FEATURES:
				return getFunctionalFeatures();
			case TfmPackage.TFM__TOPOLOGICAL_RELATIONSHIPS:
				return getTopologicalRelationships();
			case TfmPackage.TFM__LOGICAL_RELATIONSHIPS:
				return getLogicalRelationships();
			case TfmPackage.TFM__ACTORS:
				return getActors();
			case TfmPackage.TFM__CYCLES:
				return getCycles();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TfmPackage.TFM__FUNCTIONAL_FEATURES:
				getFunctionalFeatures().clear();
				getFunctionalFeatures().addAll((Collection<? extends FunctionalFeature>)newValue);
				return;
			case TfmPackage.TFM__TOPOLOGICAL_RELATIONSHIPS:
				getTopologicalRelationships().clear();
				getTopologicalRelationships().addAll((Collection<? extends TopologicalRelationship>)newValue);
				return;
			case TfmPackage.TFM__LOGICAL_RELATIONSHIPS:
				getLogicalRelationships().clear();
				getLogicalRelationships().addAll((Collection<? extends LogicalRelationship>)newValue);
				return;
			case TfmPackage.TFM__ACTORS:
				getActors().clear();
				getActors().addAll((Collection<? extends Actor>)newValue);
				return;
			case TfmPackage.TFM__CYCLES:
				getCycles().clear();
				getCycles().addAll((Collection<? extends Cycle>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TfmPackage.TFM__FUNCTIONAL_FEATURES:
				getFunctionalFeatures().clear();
				return;
			case TfmPackage.TFM__TOPOLOGICAL_RELATIONSHIPS:
				getTopologicalRelationships().clear();
				return;
			case TfmPackage.TFM__LOGICAL_RELATIONSHIPS:
				getLogicalRelationships().clear();
				return;
			case TfmPackage.TFM__ACTORS:
				getActors().clear();
				return;
			case TfmPackage.TFM__CYCLES:
				getCycles().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TfmPackage.TFM__FUNCTIONAL_FEATURES:
				return functionalFeatures != null && !functionalFeatures.isEmpty();
			case TfmPackage.TFM__TOPOLOGICAL_RELATIONSHIPS:
				return topologicalRelationships != null && !topologicalRelationships.isEmpty();
			case TfmPackage.TFM__LOGICAL_RELATIONSHIPS:
				return logicalRelationships != null && !logicalRelationships.isEmpty();
			case TfmPackage.TFM__ACTORS:
				return actors != null && !actors.isEmpty();
			case TfmPackage.TFM__CYCLES:
				return cycles != null && !cycles.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //TFMImpl
