Guide to install IDM toolset

1. Eclipse Modeling Tools latest version

2. Java JDK 1.7 or above

3. Open Eclipse and go to Help / Install Modeling Components

4. Install Operational QVT

5. Install Graphical Modeling Framework

6. Unzip IDM plugins (https://bitbucket.org/armandsslihte/idm-toolset/downloads/IDM_plugins.zip)

6. Copy plugins into Eclipse Plugins folder and start Eclipse