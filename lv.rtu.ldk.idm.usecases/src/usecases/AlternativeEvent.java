/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package usecases;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Alternative Event</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see usecases.UsecasesPackage#getAlternativeEvent()
 * @model
 * @generated
 */
public interface AlternativeEvent extends SingleEvent {
} // AlternativeEvent
