/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package usecases;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Use Cases</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link usecases.UseCases#getActors <em>Actors</em>}</li>
 *   <li>{@link usecases.UseCases#getConditions <em>Conditions</em>}</li>
 *   <li>{@link usecases.UseCases#getUseCase <em>Use Case</em>}</li>
 *   <li>{@link usecases.UseCases#getDomain <em>Domain</em>}</li>
 *   <li>{@link usecases.UseCases#getScope <em>Scope</em>}</li>
 *   <li>{@link usecases.UseCases#getOntology <em>Ontology</em>}</li>
 * </ul>
 * </p>
 *
 * @see usecases.UsecasesPackage#getUseCases()
 * @model
 * @generated
 */
public interface UseCases extends EObject {
	/**
	 * Returns the value of the '<em><b>Actors</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Actors</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Actors</em>' containment reference.
	 * @see #setActors(Actors)
	 * @see usecases.UsecasesPackage#getUseCases_Actors()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Actors getActors();

	/**
	 * Sets the value of the '{@link usecases.UseCases#getActors <em>Actors</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Actors</em>' containment reference.
	 * @see #getActors()
	 * @generated
	 */
	void setActors(Actors value);

	/**
	 * Returns the value of the '<em><b>Conditions</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Conditions</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Conditions</em>' containment reference.
	 * @see #setConditions(Conditions)
	 * @see usecases.UsecasesPackage#getUseCases_Conditions()
	 * @model containment="true"
	 * @generated
	 */
	Conditions getConditions();

	/**
	 * Sets the value of the '{@link usecases.UseCases#getConditions <em>Conditions</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Conditions</em>' containment reference.
	 * @see #getConditions()
	 * @generated
	 */
	void setConditions(Conditions value);

	/**
	 * Returns the value of the '<em><b>Use Case</b></em>' containment reference list.
	 * The list contents are of type {@link usecases.UseCase}.
	 * It is bidirectional and its opposite is '{@link usecases.UseCase#getUseCases <em>Use Cases</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Use Case</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Use Case</em>' containment reference list.
	 * @see usecases.UsecasesPackage#getUseCases_UseCase()
	 * @see usecases.UseCase#getUseCases
	 * @model opposite="useCases" containment="true" keys="id" required="true"
	 * @generated
	 */
	EList<UseCase> getUseCase();

	/**
	 * Returns the value of the '<em><b>Domain</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Domain</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Domain</em>' attribute.
	 * @see #setDomain(String)
	 * @see usecases.UsecasesPackage#getUseCases_Domain()
	 * @model required="true"
	 * @generated
	 */
	String getDomain();

	/**
	 * Sets the value of the '{@link usecases.UseCases#getDomain <em>Domain</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Domain</em>' attribute.
	 * @see #getDomain()
	 * @generated
	 */
	void setDomain(String value);

	/**
	 * Returns the value of the '<em><b>Scope</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scope</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scope</em>' attribute.
	 * @see #setScope(String)
	 * @see usecases.UsecasesPackage#getUseCases_Scope()
	 * @model required="true"
	 * @generated
	 */
	String getScope();

	/**
	 * Sets the value of the '{@link usecases.UseCases#getScope <em>Scope</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scope</em>' attribute.
	 * @see #getScope()
	 * @generated
	 */
	void setScope(String value);

	/**
	 * Returns the value of the '<em><b>Ontology</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ontology</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ontology</em>' attribute.
	 * @see #setOntology(String)
	 * @see usecases.UsecasesPackage#getUseCases_Ontology()
	 * @model required="true"
	 * @generated
	 */
	String getOntology();

	/**
	 * Sets the value of the '{@link usecases.UseCases#getOntology <em>Ontology</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ontology</em>' attribute.
	 * @see #getOntology()
	 * @generated
	 */
	void setOntology(String value);

} // UseCases
