/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package usecases;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Unbound Event</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see usecases.UsecasesPackage#getUnboundEvent()
 * @model
 * @generated
 */
public interface UnboundEvent extends SingleEvent {
} // UnboundEvent
