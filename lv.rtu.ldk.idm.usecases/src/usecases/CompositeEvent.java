/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package usecases;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Composite Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link usecases.CompositeEvent#getEvents <em>Events</em>}</li>
 *   <li>{@link usecases.CompositeEvent#getOperation <em>Operation</em>}</li>
 * </ul>
 * </p>
 *
 * @see usecases.UsecasesPackage#getCompositeEvent()
 * @model
 * @generated
 */
public interface CompositeEvent extends Event {
	/**
	 * Returns the value of the '<em><b>Events</b></em>' reference list.
	 * The list contents are of type {@link usecases.Event}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Events</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Events</em>' reference list.
	 * @see usecases.UsecasesPackage#getCompositeEvent_Events()
	 * @model lower="2"
	 * @generated
	 */
	EList<Event> getEvents();

	/**
	 * Returns the value of the '<em><b>Operation</b></em>' attribute.
	 * The literals are from the enumeration {@link usecases.Operation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operation</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operation</em>' attribute.
	 * @see usecases.Operation
	 * @see #setOperation(Operation)
	 * @see usecases.UsecasesPackage#getCompositeEvent_Operation()
	 * @model
	 * @generated
	 */
	Operation getOperation();

	/**
	 * Sets the value of the '{@link usecases.CompositeEvent#getOperation <em>Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operation</em>' attribute.
	 * @see usecases.Operation
	 * @see #getOperation()
	 * @generated
	 */
	void setOperation(Operation value);

} // CompositeEvent
