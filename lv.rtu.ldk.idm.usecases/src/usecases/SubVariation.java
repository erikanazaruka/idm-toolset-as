/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package usecases;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sub Variation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link usecases.SubVariation#getSubVariationUseCase <em>Sub Variation Use Case</em>}</li>
 * </ul>
 * </p>
 *
 * @see usecases.UsecasesPackage#getSubVariation()
 * @model
 * @generated
 */
public interface SubVariation extends AlternativeScenario {

	/**
	 * Returns the value of the '<em><b>Sub Variation Use Case</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link usecases.UseCase#getSubVariations <em>Sub Variations</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sub Variation Use Case</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Variation Use Case</em>' container reference.
	 * @see #setSubVariationUseCase(UseCase)
	 * @see usecases.UsecasesPackage#getSubVariation_SubVariationUseCase()
	 * @see usecases.UseCase#getSubVariations
	 * @model opposite="subVariations" required="true" transient="false"
	 * @generated
	 */
	UseCase getSubVariationUseCase();

	/**
	 * Sets the value of the '{@link usecases.SubVariation#getSubVariationUseCase <em>Sub Variation Use Case</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sub Variation Use Case</em>' container reference.
	 * @see #getSubVariationUseCase()
	 * @generated
	 */
	void setSubVariationUseCase(UseCase value);

} // SubVariation
