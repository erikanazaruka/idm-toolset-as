/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package usecases;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link usecases.Event#getId <em>Id</em>}</li>
 *   <li>{@link usecases.Event#getPreconditions <em>Preconditions</em>}</li>
 *   <li>{@link usecases.Event#getPostconditions <em>Postconditions</em>}</li>
 * </ul>
 * </p>
 *
 * @see usecases.UsecasesPackage#getEvent()
 * @model abstract="true"
 * @generated
 */
public interface Event extends EObject {
	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see usecases.UsecasesPackage#getEvent_Id()
	 * @model id="true" required="true"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link usecases.Event#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Preconditions</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Preconditions</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Preconditions</em>' reference.
	 * @see #setPreconditions(Condition)
	 * @see usecases.UsecasesPackage#getEvent_Preconditions()
	 * @model keys="id"
	 * @generated
	 */
	Condition getPreconditions();

	/**
	 * Sets the value of the '{@link usecases.Event#getPreconditions <em>Preconditions</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Preconditions</em>' reference.
	 * @see #getPreconditions()
	 * @generated
	 */
	void setPreconditions(Condition value);

	/**
	 * Returns the value of the '<em><b>Postconditions</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Postconditions</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Postconditions</em>' reference.
	 * @see #setPostconditions(Condition)
	 * @see usecases.UsecasesPackage#getEvent_Postconditions()
	 * @model keys="id"
	 * @generated
	 */
	Condition getPostconditions();

	/**
	 * Sets the value of the '{@link usecases.Event#getPostconditions <em>Postconditions</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Postconditions</em>' reference.
	 * @see #getPostconditions()
	 * @generated
	 */
	void setPostconditions(Condition value);

} // Event
