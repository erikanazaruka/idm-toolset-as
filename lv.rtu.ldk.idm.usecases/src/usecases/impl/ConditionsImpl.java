/**
 */
package usecases.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import usecases.CompositeCondition;
import usecases.Condition;
import usecases.Conditions;
import usecases.SingleCondition;
import usecases.UsecasesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Conditions</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link usecases.impl.ConditionsImpl#getCondition <em>Condition</em>}</li>
 *   <li>{@link usecases.impl.ConditionsImpl#getCompositeCondition <em>Composite Condition</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ConditionsImpl extends EObjectImpl implements Conditions {
	/**
	 * The cached value of the '{@link #getCondition() <em>Condition</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCondition()
	 * @generated
	 * @ordered
	 */
	protected EList<SingleCondition> condition;

	/**
	 * The cached value of the '{@link #getCompositeCondition() <em>Composite Condition</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCompositeCondition()
	 * @generated
	 * @ordered
	 */
	protected EList<CompositeCondition> compositeCondition;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConditionsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UsecasesPackage.Literals.CONDITIONS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SingleCondition> getCondition() {
		if (condition == null) {
			condition = new EObjectContainmentEList<SingleCondition>(SingleCondition.class, this, UsecasesPackage.CONDITIONS__CONDITION);
		}
		return condition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CompositeCondition> getCompositeCondition() {
		if (compositeCondition == null) {
			compositeCondition = new EObjectContainmentEList<CompositeCondition>(CompositeCondition.class, this, UsecasesPackage.CONDITIONS__COMPOSITE_CONDITION);
		}
		return compositeCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UsecasesPackage.CONDITIONS__CONDITION:
				return ((InternalEList<?>)getCondition()).basicRemove(otherEnd, msgs);
			case UsecasesPackage.CONDITIONS__COMPOSITE_CONDITION:
				return ((InternalEList<?>)getCompositeCondition()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UsecasesPackage.CONDITIONS__CONDITION:
				return getCondition();
			case UsecasesPackage.CONDITIONS__COMPOSITE_CONDITION:
				return getCompositeCondition();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UsecasesPackage.CONDITIONS__CONDITION:
				getCondition().clear();
				getCondition().addAll((Collection<? extends SingleCondition>)newValue);
				return;
			case UsecasesPackage.CONDITIONS__COMPOSITE_CONDITION:
				getCompositeCondition().clear();
				getCompositeCondition().addAll((Collection<? extends CompositeCondition>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UsecasesPackage.CONDITIONS__CONDITION:
				getCondition().clear();
				return;
			case UsecasesPackage.CONDITIONS__COMPOSITE_CONDITION:
				getCompositeCondition().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UsecasesPackage.CONDITIONS__CONDITION:
				return condition != null && !condition.isEmpty();
			case UsecasesPackage.CONDITIONS__COMPOSITE_CONDITION:
				return compositeCondition != null && !compositeCondition.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ConditionsImpl
