/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package usecases.impl;

import org.eclipse.emf.ecore.EClass;

import usecases.AlternativeEvent;
import usecases.UsecasesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Alternative Event</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class AlternativeEventImpl extends SingleEventImpl implements AlternativeEvent {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AlternativeEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UsecasesPackage.Literals.ALTERNATIVE_EVENT;
	}

} //AlternativeEventImpl
