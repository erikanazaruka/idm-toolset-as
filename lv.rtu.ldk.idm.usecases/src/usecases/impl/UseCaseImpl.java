/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package usecases.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import usecases.Actor;
import usecases.Extension;
import usecases.MainScenario;
import usecases.SubVariation;
import usecases.UseCase;
import usecases.UseCases;
import usecases.UsecasesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Use Case</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link usecases.impl.UseCaseImpl#getId <em>Id</em>}</li>
 *   <li>{@link usecases.impl.UseCaseImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link usecases.impl.UseCaseImpl#getActors <em>Actors</em>}</li>
 *   <li>{@link usecases.impl.UseCaseImpl#getMainScenario <em>Main Scenario</em>}</li>
 *   <li>{@link usecases.impl.UseCaseImpl#getExtensions <em>Extensions</em>}</li>
 *   <li>{@link usecases.impl.UseCaseImpl#getSubVariations <em>Sub Variations</em>}</li>
 *   <li>{@link usecases.impl.UseCaseImpl#getUseCases <em>Use Cases</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class UseCaseImpl extends EObjectImpl implements UseCase {
	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getActors() <em>Actors</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActors()
	 * @generated
	 * @ordered
	 */
	protected EList<Actor> actors;

	/**
	 * The cached value of the '{@link #getMainScenario() <em>Main Scenario</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMainScenario()
	 * @generated
	 * @ordered
	 */
	protected MainScenario mainScenario;

	/**
	 * The cached value of the '{@link #getExtensions() <em>Extensions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtensions()
	 * @generated
	 * @ordered
	 */
	protected EList<Extension> extensions;

	/**
	 * The cached value of the '{@link #getSubVariations() <em>Sub Variations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubVariations()
	 * @generated
	 * @ordered
	 */
	protected EList<SubVariation> subVariations;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UseCaseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UsecasesPackage.Literals.USE_CASE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UseCases getUseCases() {
		if (eContainerFeatureID() != UsecasesPackage.USE_CASE__USE_CASES) return null;
		return (UseCases)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUseCases(UseCases newUseCases, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newUseCases, UsecasesPackage.USE_CASE__USE_CASES, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUseCases(UseCases newUseCases) {
		if (newUseCases != eInternalContainer() || (eContainerFeatureID() != UsecasesPackage.USE_CASE__USE_CASES && newUseCases != null)) {
			if (EcoreUtil.isAncestor(this, newUseCases))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newUseCases != null)
				msgs = ((InternalEObject)newUseCases).eInverseAdd(this, UsecasesPackage.USE_CASES__USE_CASE, UseCases.class, msgs);
			msgs = basicSetUseCases(newUseCases, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UsecasesPackage.USE_CASE__USE_CASES, newUseCases, newUseCases));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UsecasesPackage.USE_CASE__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UsecasesPackage.USE_CASE__DESCRIPTION, oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Actor> getActors() {
		if (actors == null) {
			actors = new EObjectResolvingEList<Actor>(Actor.class, this, UsecasesPackage.USE_CASE__ACTORS);
		}
		return actors;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MainScenario getMainScenario() {
		return mainScenario;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMainScenario(MainScenario newMainScenario, NotificationChain msgs) {
		MainScenario oldMainScenario = mainScenario;
		mainScenario = newMainScenario;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, UsecasesPackage.USE_CASE__MAIN_SCENARIO, oldMainScenario, newMainScenario);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMainScenario(MainScenario newMainScenario) {
		if (newMainScenario != mainScenario) {
			NotificationChain msgs = null;
			if (mainScenario != null)
				msgs = ((InternalEObject)mainScenario).eInverseRemove(this, UsecasesPackage.MAIN_SCENARIO__MAIN_SCENARIO_USE_CASE, MainScenario.class, msgs);
			if (newMainScenario != null)
				msgs = ((InternalEObject)newMainScenario).eInverseAdd(this, UsecasesPackage.MAIN_SCENARIO__MAIN_SCENARIO_USE_CASE, MainScenario.class, msgs);
			msgs = basicSetMainScenario(newMainScenario, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UsecasesPackage.USE_CASE__MAIN_SCENARIO, newMainScenario, newMainScenario));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Extension> getExtensions() {
		if (extensions == null) {
			extensions = new EObjectContainmentWithInverseEList<Extension>(Extension.class, this, UsecasesPackage.USE_CASE__EXTENSIONS, UsecasesPackage.EXTENSION__EXTENSION_USE_CASE);
		}
		return extensions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SubVariation> getSubVariations() {
		if (subVariations == null) {
			subVariations = new EObjectContainmentWithInverseEList<SubVariation>(SubVariation.class, this, UsecasesPackage.USE_CASE__SUB_VARIATIONS, UsecasesPackage.SUB_VARIATION__SUB_VARIATION_USE_CASE);
		}
		return subVariations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UsecasesPackage.USE_CASE__MAIN_SCENARIO:
				if (mainScenario != null)
					msgs = ((InternalEObject)mainScenario).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - UsecasesPackage.USE_CASE__MAIN_SCENARIO, null, msgs);
				return basicSetMainScenario((MainScenario)otherEnd, msgs);
			case UsecasesPackage.USE_CASE__EXTENSIONS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getExtensions()).basicAdd(otherEnd, msgs);
			case UsecasesPackage.USE_CASE__SUB_VARIATIONS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getSubVariations()).basicAdd(otherEnd, msgs);
			case UsecasesPackage.USE_CASE__USE_CASES:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetUseCases((UseCases)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UsecasesPackage.USE_CASE__MAIN_SCENARIO:
				return basicSetMainScenario(null, msgs);
			case UsecasesPackage.USE_CASE__EXTENSIONS:
				return ((InternalEList<?>)getExtensions()).basicRemove(otherEnd, msgs);
			case UsecasesPackage.USE_CASE__SUB_VARIATIONS:
				return ((InternalEList<?>)getSubVariations()).basicRemove(otherEnd, msgs);
			case UsecasesPackage.USE_CASE__USE_CASES:
				return basicSetUseCases(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case UsecasesPackage.USE_CASE__USE_CASES:
				return eInternalContainer().eInverseRemove(this, UsecasesPackage.USE_CASES__USE_CASE, UseCases.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UsecasesPackage.USE_CASE__ID:
				return getId();
			case UsecasesPackage.USE_CASE__DESCRIPTION:
				return getDescription();
			case UsecasesPackage.USE_CASE__ACTORS:
				return getActors();
			case UsecasesPackage.USE_CASE__MAIN_SCENARIO:
				return getMainScenario();
			case UsecasesPackage.USE_CASE__EXTENSIONS:
				return getExtensions();
			case UsecasesPackage.USE_CASE__SUB_VARIATIONS:
				return getSubVariations();
			case UsecasesPackage.USE_CASE__USE_CASES:
				return getUseCases();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UsecasesPackage.USE_CASE__ID:
				setId((String)newValue);
				return;
			case UsecasesPackage.USE_CASE__DESCRIPTION:
				setDescription((String)newValue);
				return;
			case UsecasesPackage.USE_CASE__ACTORS:
				getActors().clear();
				getActors().addAll((Collection<? extends Actor>)newValue);
				return;
			case UsecasesPackage.USE_CASE__MAIN_SCENARIO:
				setMainScenario((MainScenario)newValue);
				return;
			case UsecasesPackage.USE_CASE__EXTENSIONS:
				getExtensions().clear();
				getExtensions().addAll((Collection<? extends Extension>)newValue);
				return;
			case UsecasesPackage.USE_CASE__SUB_VARIATIONS:
				getSubVariations().clear();
				getSubVariations().addAll((Collection<? extends SubVariation>)newValue);
				return;
			case UsecasesPackage.USE_CASE__USE_CASES:
				setUseCases((UseCases)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UsecasesPackage.USE_CASE__ID:
				setId(ID_EDEFAULT);
				return;
			case UsecasesPackage.USE_CASE__DESCRIPTION:
				setDescription(DESCRIPTION_EDEFAULT);
				return;
			case UsecasesPackage.USE_CASE__ACTORS:
				getActors().clear();
				return;
			case UsecasesPackage.USE_CASE__MAIN_SCENARIO:
				setMainScenario((MainScenario)null);
				return;
			case UsecasesPackage.USE_CASE__EXTENSIONS:
				getExtensions().clear();
				return;
			case UsecasesPackage.USE_CASE__SUB_VARIATIONS:
				getSubVariations().clear();
				return;
			case UsecasesPackage.USE_CASE__USE_CASES:
				setUseCases((UseCases)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UsecasesPackage.USE_CASE__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case UsecasesPackage.USE_CASE__DESCRIPTION:
				return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
			case UsecasesPackage.USE_CASE__ACTORS:
				return actors != null && !actors.isEmpty();
			case UsecasesPackage.USE_CASE__MAIN_SCENARIO:
				return mainScenario != null;
			case UsecasesPackage.USE_CASE__EXTENSIONS:
				return extensions != null && !extensions.isEmpty();
			case UsecasesPackage.USE_CASE__SUB_VARIATIONS:
				return subVariations != null && !subVariations.isEmpty();
			case UsecasesPackage.USE_CASE__USE_CASES:
				return getUseCases() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (id: ");
		result.append(id);
		result.append(", description: ");
		result.append(description);
		result.append(')');
		return result.toString();
	}

} //UseCaseImpl
