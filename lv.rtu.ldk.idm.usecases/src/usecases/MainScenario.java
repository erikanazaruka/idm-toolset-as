/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package usecases;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Main Scenario</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link usecases.MainScenario#getStep <em>Step</em>}</li>
 *   <li>{@link usecases.MainScenario#getMainScenarioUseCase <em>Main Scenario Use Case</em>}</li>
 * </ul>
 * </p>
 *
 * @see usecases.UsecasesPackage#getMainScenario()
 * @model
 * @generated
 */
public interface MainScenario extends Scenario {
	/**
	 * Returns the value of the '<em><b>Step</b></em>' containment reference list.
	 * The list contents are of type {@link usecases.DefaultEvent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Step</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Step</em>' containment reference list.
	 * @see usecases.UsecasesPackage#getMainScenario_Step()
	 * @model containment="true" keys="id" required="true"
	 * @generated
	 */
	EList<DefaultEvent> getStep();

	/**
	 * Returns the value of the '<em><b>Main Scenario Use Case</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link usecases.UseCase#getMainScenario <em>Main Scenario</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Main Scenario Use Case</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Main Scenario Use Case</em>' container reference.
	 * @see #setMainScenarioUseCase(UseCase)
	 * @see usecases.UsecasesPackage#getMainScenario_MainScenarioUseCase()
	 * @see usecases.UseCase#getMainScenario
	 * @model opposite="mainScenario" required="true" transient="false"
	 * @generated
	 */
	UseCase getMainScenarioUseCase();

	/**
	 * Sets the value of the '{@link usecases.MainScenario#getMainScenarioUseCase <em>Main Scenario Use Case</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Main Scenario Use Case</em>' container reference.
	 * @see #getMainScenarioUseCase()
	 * @generated
	 */
	void setMainScenarioUseCase(UseCase value);

} // MainScenario
