package lv.rtu.ldk.idm.usecases2tfm.blackbox;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.Label;
import edu.stanford.nlp.objectbank.TokenizerFactory;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import edu.stanford.nlp.process.PTBTokenizer;
import edu.stanford.nlp.process.Tokenizer;
import edu.stanford.nlp.trees.Tree;
import java.io.StringReader;
import java.util.List;

public class ParserTools
{
  private static final String NOUN_PHRASE = "NP";
  private static final String VERB_PHRASE = "VP";
  private static final String PERIOD = ".";
  private String grammar = "lib/englishPCFG.ser.gz";
  private String[] options = { "-maxLength", "80", "-retainTmpSubcategories" };
  private LexicalizedParser lp;
  private TokenizerFactory<CoreLabel> tokenizerFactory;

  public ParserTools()
  {
    this.lp = new LexicalizedParser(this.grammar, this.options);
    this.tokenizerFactory = PTBTokenizer.factory(new CoreLabelTokenFactory(), "");
  }

  public String getEntity(String sentence) {
    String result = "";

    sentence = prepareSentence(sentence);

    List rawWords = this.tokenizerFactory.getTokenizer(new StringReader(sentence)).tokenize();
    Tree parse = this.lp.apply(rawWords);

    for (Tree tree : parse.children()[0].children())
    {
      if (tree.label().value().equalsIgnoreCase("NP")) {
        
    	  //Check if the Noun Phrase consists of several words
    	  if (tree.getLeaves().size() == 1) {
    		  //Return the single word to be checked as the entity
    		  result = ((Tree)tree.getLeaves().get(0)).label().value();
    	  } else {
    		  //Return multiple words to be checked as the entity
    		  for (int i = 0; i < tree.getLeaves().size(); i++) {
    			  result = result + ((Tree)tree.getLeaves().get(i)).label().value() + " ";
    		  }
    		  result = result.trim();
    	  }
    	  
    	  

      }
    }

    return result;
  }

  public String getDescription(String sentence) {
    String result = "";

    sentence = prepareSentence(sentence);

    List rawWords = this.tokenizerFactory.getTokenizer(new StringReader(sentence)).tokenize();
    Tree parse = this.lp.apply(rawWords);

    for (Tree tree : parse.children()[0].children()) {
      if (tree.label().value().equalsIgnoreCase("VP")) {
        result = resolveTree(tree).trim();
      }
    }

    if (result.substring(0, 2).equalsIgnoreCase("is")) {
      result = result.substring(3);
    }

    return result;
  }

  private String resolveTree(Tree tree) {
    String result = "";

    if (tree.children().length > 0) {
      for (Tree t : tree.children())
        result = result + resolveTree(t);
    }
    else {
      result = tree.label().value();
    }

    return result.trim() + " ";
  }

  public void analyzeString(String sentence)
  {
    sentence = prepareSentence(sentence);

    List rawWords = this.tokenizerFactory.getTokenizer(new StringReader(sentence)).tokenize();
    Tree parse = this.lp.apply(rawWords);

    parse.pennPrint();
  }

  private String prepareSentence(String sentence)
  {
    if (sentence.endsWith(".")) {
      return sentence;
    }
    return sentence + ".";
  }
}