package lv.rtu.ldk.idm.usecases2tfm.blackbox;

public class UtilitiesLibrary
{
  private static ParserTools parserTools = new ParserTools();

  public static String parseStepForEntity(String stepDesciption)
  {
    return parserTools.getEntity(stepDesciption);
  }

  public static String parseStepForDescription(String stepDesciption) {
    return parserTools.getDescription(stepDesciption);
  }
}